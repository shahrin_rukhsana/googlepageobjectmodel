package lib;

import java.io.FileInputStream;

import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {
	Sheet sh;
	
	public  ExcelReader(String fileName,String sheetName) throws IOException {
		FileInputStream fis = new FileInputStream(fileName);
		Workbook wb=new XSSFWorkbook(fis);
		sh=wb.getSheet(sheetName);
	}
	
	public Object[][] excelToArray() {
		Object[][]t;
		int totalRows=sh.getPhysicalNumberOfRows();
		int totalCols=sh.getRow(0).getPhysicalNumberOfCells();
		t=new Object[totalRows-1][totalCols];
		for(int r=1;r<totalRows;r=r+1) {
			for(int c=0;c<totalCols;c=c+1) {
				t[r-1][c]=getCellData(r, c);
				
				
			}
		}
		
		return t;
	}
	
	public String getCellData(int row,int col) {
		Cell c=sh.getRow(row).getCell(col);
		String v="";
		if(c.getCellType()==Cell.CELL_TYPE_STRING) {
			v=c.getStringCellValue();
		}
		else if (c.getCellType()==Cell.CELL_TYPE_NUMERIC) {
			if(c.getNumericCellValue()%1==0) {
				v=""+(long)c.getNumericCellValue();
				
			}
			else {
				v=""+c.getNumericCellValue();
			}
			
		}
		return v;
		
	}
}
