package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import models.CreatGoogleAccountModel;

public class CreatGoogleAccountPage extends CreatGoogleAccountModel {

	public CreatGoogleAccountPage(WebDriver driver) {
		super(driver);
		
	}
	
	public void typeFirstName(String firstName) {

		WebElement e=getFirstName();
		e.clear();
		e.sendKeys(firstName);
	}
	
	public void typeLastName(String lastName) {

		WebElement e=getLastName();
		e.clear();
		e.sendKeys(lastName);
	}
	
	public void typeUserName(String userName) {

		WebElement e=getUserName();
		e.clear();
		e.sendKeys(userName);
	}
	
	public void typePassWord(String passWord) {

		WebElement e=getPassWord();
		e.clear();
		e.sendKeys(passWord);
	}
	
	public void typeConfirmPassWord(String confirmPass) {

		WebElement e=getConfirmPassWord();
		e.clear();
		e.sendKeys(confirmPass);
	}
	
	public void checkShowPassWord() {
		WebElement e= getShowPassWord();
		e.click();
	}
	
	public void clickOnNext() {
		WebElement e= getNext();
		e.click();
	}





}
