package pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.*;
import org.openqa.selenium.WebElement;

import models.VerifyGooglePhoneModel;

public class VerifyGooglePhonePage extends VerifyGooglePhoneModel {

	public VerifyGooglePhonePage(WebDriver driver) {
		super(driver);
		
	}
	
	public void typePhoneNumber(String number) throws InterruptedException {
		Thread.sleep(1000);
		WebElement e = getPhoneNumber();
		e.click();
		e.clear();
		e.sendKeys(number);
		
	}
	
	public void clickOnNext() {
		WebElement e=getNextButton();
		e.click();
	}
	
	public void verifyPhoneNo(String phNo) {
		String a=phNo.substring(0, 3);
		String b=phNo.substring(3, 6);
		String c=phNo.substring(6, 10);
		
		String actPh = "("+a+") "+b+"-"+c;
		String text=getVerifyPhoneNumber().getText();
		assertEquals(text, actPh);
	}

}
