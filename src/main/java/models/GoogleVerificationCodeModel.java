package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class GoogleVerificationCodeModel extends BaseModelGoogle {

	public GoogleVerificationCodeModel(WebDriver driver) {
		super(driver);


	}
	
	public WebElement getPhoneCode() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@jsname='wKtwcc']")));
		WebElement e =driver.findElement(By.xpath("//span[@jsname='wKtwcc']"));
		return e;
		
	}

}
