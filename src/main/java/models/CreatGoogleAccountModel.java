package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CreatGoogleAccountModel extends BaseModelGoogle {

	public CreatGoogleAccountModel(WebDriver driver) {
		super(driver);
		
	}
	
	public WebElement getFirstName() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='firstName']")));
		WebElement e =driver.findElement(By.xpath("//input[@name='firstName']"));
		return e;
	}
	
	public WebElement getLastName() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='lastName']")));
		WebElement e =driver.findElement(By.xpath("//input[@name='lastName']"));
		return e;
	}
	
	public WebElement getUserName() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='Username']")));
		WebElement e =driver.findElement(By.xpath("//input[@name='Username']"));
		return e;
	}
	public WebElement getPassWord() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='Passwd']")));
		WebElement e =driver.findElement(By.xpath("//input[@name='Passwd']"));
		return e;
	}
	public WebElement getConfirmPassWord() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='ConfirmPasswd']")));
		WebElement e =driver.findElement(By.xpath("//input[@name='ConfirmPasswd']"));
		return e;
	}
	
	public WebElement getShowPassWord() {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='checkbox']")));
		WebElement e =driver.findElement(By.xpath("//input[@type='checkbox']"));
		return e;
	}
	
	
	public WebElement getNext() {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Next')]/ancestor::button")));
		WebElement e =driver.findElement(By.xpath("//span[contains(text(),'Next')]/ancestor::button"));
		return e;
	}


}
