package models;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseModelGoogle {
	
	WebDriver driver;
	WebDriverWait wait;
	
	public BaseModelGoogle(WebDriver driver) {
		
		this. driver= driver;
		wait = new WebDriverWait(driver, 20);
		
	}
	

}
