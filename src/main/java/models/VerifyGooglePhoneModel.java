package models;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class VerifyGooglePhoneModel extends BaseModelGoogle {

	public VerifyGooglePhoneModel(WebDriver driver) {
		super(driver);
		
	}
	
	public WebElement getPhoneNumber() {
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input")));
		WebElement e =driver.findElement(By.xpath("//input"));
		return e;
	}
	
	public WebElement getNextButton() {

		WebElement e =driver.findElement(By.xpath("//span[text()='Next']/ancestor::button"));
		return e;
		
	}
	
	public WebElement getVerifyPhoneNumber() {
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='guUJvc']/child::span")));
		WebElement e=driver.findElement(By.xpath("//div[@class='guUJvc']/child::span"));
		return e;
	}

}
