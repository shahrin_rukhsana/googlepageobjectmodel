package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import pages.CreatGoogleAccountPage;
import pages.VerifyGooglePhonePage;

public class BaseGoogleTest {
	
	static WebDriver driver;
	static CreatGoogleAccountPage cgap;
	static VerifyGooglePhonePage vgpp;
	
	@BeforeSuite
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://accounts.google.com/signup/v2");
		driver.manage().window().maximize();
		
	}
	public void getCreatGoogleAccountPage() {
		cgap=new CreatGoogleAccountPage(driver);
	}
	
	public void getVerifyGooglePhonePage() {
		vgpp=new VerifyGooglePhonePage(driver);
		
	}
	
	@AfterSuite
	public void tearDown() {
		
		//driver.quit();
	}

}
