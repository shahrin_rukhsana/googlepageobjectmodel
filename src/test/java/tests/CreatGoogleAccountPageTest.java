package tests;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import lib.ExcelReader;

public class CreatGoogleAccountPageTest extends BaseGoogleTest {
	
	
	@Test(dataProvider="gmail")
	
	public void creatGoogleAccountPageTest(String firstName,String lastName,String userName,
			String passWord,String confirmPass,String phone) {
		getCreatGoogleAccountPage();
		cgap.typeFirstName(firstName);
		cgap.typeLastName(lastName);
		cgap.typeUserName(userName);
		cgap.typePassWord(passWord);
		cgap.typeConfirmPassWord(confirmPass);
		cgap.checkShowPassWord();
		cgap.clickOnNext();
		
		
		
		
	}
	
@DataProvider(name="gmail")
	
	public Object[][]getDataLoader() throws IOException{
		
		Object[][] t;
		
		String fileName="data/gmail.xlsx";
		String sheetName="gmail";
		ExcelReader er = new ExcelReader(fileName, sheetName);
		t=er.excelToArray();
		return t;
		
	}


}
