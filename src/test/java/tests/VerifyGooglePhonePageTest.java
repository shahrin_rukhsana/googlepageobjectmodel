package tests;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import lib.ExcelReader;

public class VerifyGooglePhonePageTest extends BaseGoogleTest {
	
@Test(dataProvider="gmail")
	
	public void verifyGooglePhonePageTest(String firstName,String lastName,String userName,
			String passWord,String confirmPass,String number) throws InterruptedException {
		getCreatGoogleAccountPage();
		cgap.typeFirstName(firstName);
		cgap.typeLastName(lastName);
		cgap.typeUserName(userName);
		cgap.typePassWord(passWord);
		cgap.typeConfirmPassWord(confirmPass);
		cgap.checkShowPassWord();
		cgap.clickOnNext();
		getVerifyGooglePhonePage();
		vgpp.typePhoneNumber(number);
		vgpp.clickOnNext();
		vgpp.verifyPhoneNo(number);
		
		
		
		
	}
	
@DataProvider(name="gmail")
	
	public Object[][]getDataLoader() throws IOException{
		
		Object[][] t;
		
		String fileName="data/gmail.xlsx";
		String sheetName="gmail";
		ExcelReader er = new ExcelReader(fileName, sheetName);
		t=er.excelToArray();
		return t;
		
	}


}
